from components.emitter import Emitter

emitter = Emitter('task_queue')

filename = 'generate_import_data.csv'

with open(f'data/{filename}', 'r') as f:
    filedata = f.read()
    f.close()

payload = {
    'filedata': filedata
}

emitter.add_header('filename', filename)
emitter.emit('blockchain.add.import', payload)

while True:
    current = emitter.get_current_job_status()
    if current['status'] == 'SUCCESS':
        print(current)
        break