from io import StringIO
import csv

class ManageData:
    
    def __init__(self, db, config) -> None:
        self.db = db
        self.config = config
    
    def csv_to_list(self, csv_str):
        f = StringIO(csv_str)
        result = []

        header = f.readline().replace('\n', '')
        header_data = header.split(',')

        for line in csv.reader(f, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True):
            for index, element in enumerate(line):
                element = element.replace('\n', '')
                if '[' in element:
                    line[index] = element.strip('][').replace("'", "").replace('"', "").split(', ')
        
            d = dict(zip(header_data, line))
            result.append(d)
        
        return result

    async def add_product_data(self, csv_str):
        data = self.csv_to_list(csv_str)
        result = []
        
        for doc in data:
            res = await self.db.database['product'].insert_one(doc)
            result.append(str(res.inserted_id))
        
        return result
    
    async def add_recall_data(self, csv_str):
        data = self.csv_to_list(csv_str)
        result = []

        for doc in data:
            res = await self.db.database['recall'].insert_one(doc)
            result.append(str(res.inserted_id))
        
        return result