class RecallProcess:

    def __init__(self, db, config) -> None:
        self.db = db
        self.config = config
    
    async def get_recall_by_id(self, id):
        query = {
            'recall_id': id
        }

        res = await self.db.database['recall'].find_one(query)
        res['_id'] = str(res['_id'])
        return dict(res)
    
    async def get_recall_by_gtin(self, gtin):
        query = {
            'gtin': gtin
        }

        res = []

        cursor = self.db.database['recall'].find(query)
        for result in await cursor.to_list(length=100):
            result['_id'] = str(result['_id'])
            result_dict = dict(result)
            res.append(result_dict)
        
        return res
    
    async def get_active_recall_by_gtin(self, gtin):
        query = {
            'gtin': gtin,
            'info.date_end': ''
        }

        res = []

        cursor = self.db.database['recall'].find(query)
        for result in await cursor.to_list(length=100):
            result['_id'] = str(result['_id'])
            result_dict = dict(result)

            for i, info in enumerate(result_dict['info']):
                if info['date_end'] != '':
                    result_dict['info'].pop(i)

            res.append(result_dict)
        
        if len(res) > 0:
            return res[0]
        else:
            return {}