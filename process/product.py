class ProductProcess:

    def __init__(self, db, config) -> None:
        self.db = db
        self.config = config
    
    async def get_product_by_gtin(self, gtin):
        query = {
            'gtin': gtin
        }

        res = []

        cursor = self.db.database['product'].find(query)
        for result in await cursor.to_list(length=100):
            result['_id'] = str(result['_id'])
            result_dict = dict(result)
            res.append(result_dict)
        
        return res
