import uuid
import redis
import pickle

from components.emitter import Emitter

class BlockchainProcess:
    
    def __init__(self, config):
        self.emitter = Emitter(config['worker']['queue_name'])
        self.r = redis.Redis(host='localhost', port=6379, db=0)
    
    def dispatch_track_trace_file_to_blockchain(self, filename, filedata):
        payload = {
            'filedata': filedata
        }

        self.emitter.add_header('filename', filename)
        task_id = self.emitter.emit('blockchain.add.track_trace', payload)
        self.emitter.reset_data()

        return task_id
    
    def dispatch_import_file_to_blockchain(self, filename, filedata):
        payload = {
            'filedata': filedata
        }

        self.emitter.add_header('filename', filename)
        task_id = self.emitter.emit('blockchain.add.import', payload)
        self.emitter.reset_data()

        return task_id
    
    def dispatch_product_file_to_blockchain(self, filename, filedata):
        payload = {
            'filedata': filedata
        }

        self.emitter.add_header('filename', filename)
        task_id = self.emitter.emit('blockchain.add.product', payload)
        self.emitter.reset_data()

        return task_id

    def get_task_status(self, task_id):
        return pickle.loads(self.r.get(task_id))

    # DEPRECATED! REMOVED SOON!

    def add_data_to_blockchain(self, data):
        payload = {
            'generator_gln': str(data['payload']['generator_gln']),
            'event_type': str(data['payload']['event_type']),
            'input_gtin': str(data['payload']['input_gtin']),
            'output_gtin': str(data['payload']['output_gtin']),
            'serial_number': str(data['payload']['serial_number']),
            'longitude': str(data['payload']['longitude']),
            'latitude': str(data['payload']['latitude']),
            'location_name': str(data['payload']['location_name']),
            'company_name': str(data['payload']['company_name'])
        }

        task_id = self.emitter.emit('blockchain.add.data', payload)
        self.emitter.reset_data()

        return task_id
    
    # DEPRECATED! REMOVED SOON!

    def get_owner_credit_point(self, owner_id):
        payload = {
            'owner_id': owner_id
        }

        task_id = self.emitter.emit('blockchain.get.credit', payload)
        self.emitter.reset_data()

        return task_id