from dateutil import parser

class TraceProcess:

    def __init__(self, db, config) -> None:
        self.db = db
        self.config = config
    
    async def check_id_exist(self, target_id, list_of_object):
        for obj in list_of_object:
            if obj['_id'] == target_id:
                return True
        return False

    async def query_gtin(self, gtin):
        res = []
        query = {
            '$or':[
                {
                    'gtin': gtin
                },
                {
                    'input_gtin': gtin
                },
                {
                    'output_gtin': gtin
                }
            ]
        }

        cursor = self.db.database['track_trace'].find(query)
        for result in await cursor.to_list(length=100):
            result_dict = dict(result)
            res.append(result_dict)
        
        return res
    
    async def trace_gtin(self, gtin):
        result = await self.query_gtin(gtin)

        for key, d in enumerate(result):
            d['event_time'] = parser.parse(d['event_time'])
        
        # Sort List by time
        result = sorted(result, key=lambda d: d['event_time'])

        for key, d in enumerate(result):
            # Handle Object ID (Convert to String)
            d['_id'] = str(d['_id'])

            # Handle Datetime
            d['event_time'] = d['event_time'].isoformat()

            result[key] = d
        return result

    async def trace_gtin_full_path(self, gtin):
        queue_list = await self.query_gtin(gtin)
        final_list = []

        while len(queue_list) > 0:
            next_queue = []
            for doc in queue_list:
                doc['event_type'] = int(doc['event_type'])
                if not await self.check_id_exist(doc['_id'], final_list):
                    # Treat the doc date
                    doc['event_time'] = parser.parse(doc['event_time'])

                    final_list.append(doc)
                
                    if doc['event_type'] == 4:
                        # Input GTIN
                        for input_gtin in doc['input_gtin']:
                            next_queue = next_queue + await self.query_gtin(input_gtin)
                        
                        # Output GTIN
                        for output_gtin in doc['output_gtin']:
                            next_queue = next_queue + await self.query_gtin(output_gtin)

                    elif doc['event_type'] == 5:
                        # Input GTIN
                        next_queue = next_queue + await self.query_gtin(doc['input_gtin'])

                        # Output GTIN
                        next_queue = next_queue + await self.query_gtin(doc['output_gtin'])
            
            queue_list = next_queue
        
        # Sort List by time
        final_list = sorted(final_list, key=lambda d: d['event_time'])

        for key, d in enumerate(final_list):
            # Handle Object ID (Convert to String)
            d['_id'] = str(d['_id'])

            # Handle Datetime
            d['event_time'] = d['event_time'].isoformat()

            final_list[key] = d

        return final_list