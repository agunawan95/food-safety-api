import csv
import motor.motor_asyncio

conn_str = "mongodb://localhost:27017/track_trace"
client = motor.motor_asyncio.AsyncIOMotorClient(conn_str, serverSelectionTimeoutMS=5000)

db = client['food_safety']

async def add_document(d, collection):
    result = await db[collection].insert_one(d)
    print('result %s' % repr(result.inserted_id))

def migrate_usda_data():
    input_file = 'data/generate_usda_data.csv'

    with open(input_file, 'r') as f:
        header = f.readline().replace('\n', '').replace('\r', '')
        header_data = header.split(',')

        for line in csv.reader(f, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True):
            for index, element in enumerate(line):
                element = element.replace('\n', '')
                if '[' in element:
                    line[index] = element.strip('][').replace("'", "").replace('"', "").split(', ')
        
            d = dict(zip(header_data, line))

            loop = client.get_io_loop()
            loop.run_until_complete(add_document(d, 'recall'))

def migrate_product_data():
    input_file = 'data/generate_product_data.csv'

    with open(input_file, 'r') as f:
        header = f.readline().replace('\n', '').replace('\r', '')
        header_data = header.split(',')

        for line in csv.reader(f, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True):
            for index, element in enumerate(line):
                element = element.replace('\n', '')
                if '[' in element:
                    line[index] = element.strip('][').replace("'", "").replace('"', "").split(', ')
        
            d = dict(zip(header_data, line))

            loop = client.get_io_loop()
            loop.run_until_complete(add_document(d, 'product'))

def migrate_import_data():
    input_file = 'data/generate_import_data.csv'

    with open(input_file, 'r') as f:
        header = f.readline().replace('\n', '').replace('\r', '')
        header_data = header.split(',')

        for line in csv.reader(f, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True):
            for index, element in enumerate(line):
                element = element.replace('\n', '')
                if '[' in element:
                    line[index] = element.strip('][').replace("'", "").replace('"', "").split(', ')
        
            d = dict(zip(header_data, line))

            loop = client.get_io_loop()
            loop.run_until_complete(add_document(d, 'import'))

migrate_usda_data()
migrate_product_data()
migrate_import_data()