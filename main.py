import configparser
from sanic import Sanic

from components.database import Database

from views.v1.trace import trace_v1_bp
from views.v1.manage import manage_v1_bp
from views.v1.assets import assets_v1_bp
from views.v1.product import product_v1_bp
from views.v1.recall import recall_v1_bp
from views.v1.blockchain import blockchain_v1_bp

# ============================================================================================================
# ---------------------------------------- CREATE APP & CONFIGURATION ----------------------------------------
# ============================================================================================================
config = configparser.ConfigParser()
config.read('app.conf')

app = Sanic(config['app']['app_name'])

@app.before_server_start
async def setup_db(app):
    app.ctx.db = Database(config['database'])

@app.before_server_start
async def setup_config(app):
    app.config.FORWARDED_SECRET = config['app']['forwarded_secret']
    app.config.app_config = config

# ============================================================================================================
# ------------------------------------------ APP BLUEPRINT REGISTER ------------------------------------------
# ============================================================================================================

app.blueprint(trace_v1_bp)
app.blueprint(manage_v1_bp)
app.blueprint(assets_v1_bp)
app.blueprint(product_v1_bp)
app.blueprint(recall_v1_bp)
app.blueprint(blockchain_v1_bp)

# ============================================================================================================
# -------------------------------------------- MAIN APP / FUNCTION -------------------------------------------
# ============================================================================================================

if __name__ == '__main__':
    app.run(
        host=config['app']['app_host'], 
        port=int(config['app']['app_port']), 
        workers=8, 
        access_log=False
    )