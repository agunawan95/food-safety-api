from sanic.response import json as json_response
from sanic import Blueprint

from process import BlockchainProcess

blockchain_v1_bp = Blueprint('blockchain_v1_blueprint', url_prefix='/blockchain', version=1, version_prefix='/api/v')

@blockchain_v1_bp.route("/product", methods=['POST'])
def add_product_file_to_blockchain(request):
    app = request.app

    filename = request.files["file"][0].name
    filedata = request.files["file"][0].body.decode('utf-8')

    blockchain_process = BlockchainProcess(app.config.app_config)
    task_id = blockchain_process.dispatch_product_file_to_blockchain(filename=filename, filedata=filedata)

    result = {
        'task_id': task_id
    }

    return json_response(result)

@blockchain_v1_bp.route("/status/<task_id>", methods=['GET'])
def check_upload_statusp(request, task_id):
    app = request.app

    blockchain_process = BlockchainProcess(app.config.app_config)
    status = blockchain_process.get_task_status(task_id)

    return json_response(status)

# DEPRECATED MIGHT REMOVE SOON!

@blockchain_v1_bp.route("/credit/<owner_id>", methods=['GET'])
def get_owner_credit_point(request, owner_id):
    app = request.app
    
    blockchain_process = BlockchainProcess(app.config.app_config)
    task_id = blockchain_process.get_owner_credit_point(owner_id)

    result = {
        'task_id': task_id
    }

    return json_response(result)