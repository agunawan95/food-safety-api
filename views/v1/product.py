from sanic.response import json as json_response
from sanic import Blueprint

from process import ProductProcess
from process import RecallProcess

product_v1_bp = Blueprint('product_v1_blueprint', url_prefix='/product', version=1, version_prefix='/api/v')

@product_v1_bp.route("/<gtin>", methods=['GET'])
async def get_product_by_gtin(request, gtin):
    app = request.app

    product_process = ProductProcess(app.ctx.db, app.config.app_config)
    res = await product_process.get_product_by_gtin(gtin)

    return json_response(res)

@product_v1_bp.route("/full/<gtin>", methods=['GET'])
async def get_full_product_by_gtin(request, gtin):
    app = request.app

    product_process = ProductProcess(app.ctx.db, app.config.app_config)
    res = await product_process.get_product_by_gtin(gtin)
    res = res[-1]
    
    recall_process = RecallProcess(app.ctx.db, app.config.app_config)
    recall_data = await recall_process.get_active_recall_by_gtin(gtin)

    res['recall'] = recall_data

    return json_response(res)
