from sanic.response import json as json_response
from sanic import Blueprint

from process import RecallProcess

recall_v1_bp = Blueprint('recall_v1_blueprint', url_prefix='/recall', version=1, version_prefix='/api/v')

@recall_v1_bp.route("/<id>")
async def get_recall_by_id(request, id):
    app = request.app

    recall_process = RecallProcess(app.ctx.db, app.config.app_config)
    res = await recall_process.get_recall_by_id(id)

    return json_response(res)

@recall_v1_bp.route("/gtin/<gtin>")
async def get_recall_by_id(request, gtin):
    app = request.app

    recall_process = RecallProcess(app.ctx.db, app.config.app_config)
    res = await recall_process.get_recall_by_gtin(gtin)

    return json_response(res)