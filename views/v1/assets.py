from sanic.response import file, json
from sanic import Blueprint


assets_v1_bp = Blueprint('assets_v1_blueprint', url_prefix='/assets', version=1, version_prefix='/api/v')

@assets_v1_bp.route('/recall/<filename>', methods=['GET'])
async def get_recall_image_data(request, filename):
    headers = {
        'Content-Type': 'application/pdf',
        'Content-Disposition': f'inline; filename={filename}'
    }
    response = await file(f'assets/{filename}', headers=headers)

    return response

@assets_v1_bp.route('/recall', methods=['POST'])
async def post_recall_image_data(request):
    filename = request.files["file"][0].name
    filedata = request.files["file"][0].body

    with open(f"assets/{filename}", "wb") as f:
        f.write(filedata)
        f.close()
    
    result = {
        'filename': filename
    }

    return json(result)

@assets_v1_bp.route('/product/<filename>', methods=['GET'])
async def get_product_image_data(request, filename):
    headers = {
        'Content-Type': 'image/jpg',
        'Content-Disposition': f'inline; filename={filename}'
    }
    response = await file(f'assets/product_img/{filename}', headers=headers)

    return response

@assets_v1_bp.route('/product/qr/<gtin>', methods=['GET'])
async def get_product_qr(request, gtin):
    headers = {
        'Content-Type': 'image/jpg',
        'Content-Disposition': f'inline; filename={gtin}.png'
    }
    response = await file(f'assets/product_qr/{gtin}.png', headers=headers)

    return response

@assets_v1_bp.route('/trace/qr/<gtin>', methods=['GET'])
async def get_product_qr(request, gtin):
    headers = {
        'Content-Type': 'image/jpg',
        'Content-Disposition': f'inline; filename={gtin}.png'
    }
    response = await file(f'assets/trace_qr/{gtin}.png', headers=headers)

    return response