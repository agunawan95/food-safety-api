from sanic.response import json as json_response
from sanic import Blueprint

from process import ManageData

manage_v1_bp = Blueprint('manage_v1_blueprint', url_prefix='/manage', version=1, version_prefix='/api/v')

@manage_v1_bp.route("/product", methods=['POST'])
async def add_file_to_product(request):
    app = request.app

    filename = request.files["file"][0].name
    filedata = request.files["file"][0].body.decode('utf-8')

    manage_data = ManageData(app.ctx.db, app.config.app_config)
    inserted_ids = await manage_data.add_product_data(filedata)

    return json_response(inserted_ids)

@manage_v1_bp.route("/recall", methods=['POST'])
async def add_file_to_recall(request):
    app = request.app

    filename = request.files["file"][0].name
    filedata = request.files["file"][0].body.decode('utf-8')

    manage_data = ManageData(app.ctx.db, app.config.app_config)
    inserted_ids = await manage_data.add_recall_data(filedata)

    return json_response(inserted_ids)