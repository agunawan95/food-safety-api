from sanic.response import json as json_response
from sanic import Blueprint

from process import TraceProcess

trace_v1_bp = Blueprint('trace_v1_blueprint', url_prefix='/trace', version=1, version_prefix='/api/v')

@trace_v1_bp.route('/<gtin>', methods=['GET'])
async def gtin_trace(request, gtin):
    app = request.app
    trace_process = TraceProcess(app.ctx.db, app.config.app_config)
    res = await trace_process.trace_gtin(gtin)
    return json_response(res)

@trace_v1_bp.route('/full/<gtin>', methods=['GET'])
async def gtin_full_trace(request, gtin):
    app = request.app
    trace_process = TraceProcess(app.ctx.db, app.config.app_config)
    res = await trace_process.trace_gtin_full_path(gtin)
    return json_response(res)