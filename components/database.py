import motor.motor_asyncio

class Database:

    def __init__(self, db_config) -> None:
        conn_str = f"mongodb://{db_config['host']}:{db_config['port']}/{db_config['dbname']}"
        
        self.client = motor.motor_asyncio.AsyncIOMotorClient(conn_str, serverSelectionTimeoutMS=5000)
        self.database = self.client[db_config['dbname']]