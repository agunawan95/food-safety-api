import pika
import uuid
import redis
import pickle
import json

class Emitter:

    def __init__(self, queue_name) -> None:
        self.queue_name = queue_name
        
        self.reset_data()
        self.initialize_redis()
        self.initlialize_queue()
    
    def initialize_redis(self):
        self.r = redis.Redis(host='localhost', port=6379, db=0)
    
    def initlialize_queue(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()

        self.channel.queue_declare(queue=self.queue_name, durable=True)

    def reset_data(self):
        self.task_id = str(uuid.uuid4())
        self.envelope = {
            'header': {
                'task_id': self.task_id,
                'func_tag': ''
            },
            'payload': None
        }

        self.result_data = {
            'status': 'INITIALIZE',
            'result': None
        }

    def add_header(self, key, value):
        self.envelope['header'][key] = value

    def emit(self, func_tag, data):
        self.envelope['payload'] = data

        # Create Job Result in Redis
        self.r.set(self.task_id, pickle.dumps(self.result_data))

        # Send Job to Queue
        self.envelope['header']['func_tag'] = func_tag
        message = json.dumps(self.envelope)
        print (f"Emit: {message}")

        self.channel.basic_publish(
            exchange='',
            routing_key=self.queue_name,
            body=message,
            properties=pika.BasicProperties(
                delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE
            )
        )

        return self.task_id
    
    def get_current_job_status(self):
        return pickle.loads(self.r.get(self.task_id))
    
    def get_current_task_id(self):
        return self.task_id